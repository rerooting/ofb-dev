<?php
/**
 * @file
 * ofb_simple_faq.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ofb_simple_faq_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'faq';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'faq' => 'faq',
      ),
    ),
    'node' => array(
      'values' => array(
        'frequently_asked_question' => 'frequently_asked_question',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'faq/' => 'faq/',
        'faq/*' => 'faq/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-aefb000727717838cdc758d4891a3dd4' => array(
          'module' => 'views',
          'delta' => 'aefb000727717838cdc758d4891a3dd4',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'contact_form',
    ),
  );
  $context->condition_mode = 0;
  $export['faq'] = $context;

  return $export;
}
