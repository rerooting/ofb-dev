<?php
/**
 * @file
 * ofb_services_simple.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_services_simple_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ofb_services_simple_node_info() {
  $items = array(
    'services_section' => array(
      'name' => t('Services Section'),
      'base' => 'node_content',
      'description' => t('A listing of services for specialty areas.'),
      'has_title' => '1',
      'title_label' => t('Services Section Title'),
      'help' => '',
    ),
  );
  return $items;
}
