<?php
/**
 * @file
 * ofb_staff_directory.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ofb_staff_directory_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_staff_directory';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_staff_directory'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_staff_directory';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_staff_directory'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_staff_directory';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_staff_directory'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_staff_directory';
  $strongarm->value = '1';
  $export['node_preview_staff_directory'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_staff_directory';
  $strongarm->value = 0;
  $export['node_submitted_staff_directory'] = $strongarm;

  return $export;
}
