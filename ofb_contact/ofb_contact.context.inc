<?php
/**
 * @file
 * ofb_contact.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ofb_contact_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact-us';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'defaultcontent' => array(
      'values' => array(
        'contact-us' => 'contact-us',
      ),
    ),
    'menu' => array(
      'values' => array(
        'node/6' => 'node/6',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-contact-contact-info' => array(
          'module' => 'bean',
          'delta' => 'contact-contact-info',
          'region' => 'sidebar_first',
          'weight' => '-16',
        ),
        'bean-contact-map' => array(
          'module' => 'bean',
          'delta' => 'contact-map',
          'region' => 'sidebar_first',
          'weight' => '-15',
        ),
      ),
    ),
    'delta' => array(
      'delta_template' => 'contact_form',
    ),
  );
  $context->condition_mode = 0;
  $export['contact-us'] = $context;

  return $export;
}
