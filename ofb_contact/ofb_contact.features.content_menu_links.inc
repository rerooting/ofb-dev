<?php
/**
 * @file
 * ofb_contact.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function ofb_contact_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: main-menu:node/6
  $menu_links['main-menu:node/6'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node-name/contact-us',
    'router_path' => 'node/%',
    'link_title' => 'Contact Us',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Contact Us');


  return $menu_links;
}
