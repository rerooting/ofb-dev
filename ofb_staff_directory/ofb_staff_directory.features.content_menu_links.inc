<?php
/**
 * @file
 * ofb_staff_directory.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function ofb_staff_directory_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: main-menu:node/9
  $menu_links['main-menu:node/9'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node-name/sample-staff-directory',
    'router_path' => 'node/%',
    'link_title' => 'Staff',
    'options' => array(),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Staff');


  return $menu_links;
}
