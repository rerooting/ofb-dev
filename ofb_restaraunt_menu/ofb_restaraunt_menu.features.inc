<?php
/**
 * @file
 * ofb_restaraunt_menu.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_restaraunt_menu_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ofb_restaraunt_menu_node_info() {
  $items = array(
    'cafe_menu' => array(
      'name' => t('Cafe Menu'),
      'base' => 'node_content',
      'description' => t('This is a content type for a cafe menu'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
