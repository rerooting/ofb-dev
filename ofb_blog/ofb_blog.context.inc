<?php
/**
 * @file
 * ofb_blog.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function ofb_blog_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'blog';
  $context->description = 'Context for all blog articles and sections.';
  $context->tag = 'Content';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'blog' => 'blog',
      ),
    ),
    'node' => array(
      'values' => array(
        'blog_article' => 'blog_article',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'blog/' => 'blog/',
        'blog/*' => 'blog/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-popular_articles-block' => array(
          'module' => 'views',
          'delta' => 'popular_articles-block',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'views-blog_categories-block' => array(
          'module' => 'views',
          'delta' => 'blog_categories-block',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'views-blog_archive-block' => array(
          'module' => 'views',
          'delta' => 'blog_archive-block',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Context for all blog articles and sections.');
  $export['blog'] = $context;

  return $export;
}
