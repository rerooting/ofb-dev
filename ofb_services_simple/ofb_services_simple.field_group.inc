<?php
/**
 * @file
 * ofb_services_simple.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ofb_services_simple_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_content_right|field_collection_item|field_services|default';
  $field_group->group_name = 'group_content_right';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_services';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Content Right',
    'weight' => '3',
    'children' => array(
      0 => 'field_service_item_description',
      1 => 'services_section_title',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Content Right',
      'instance_settings' => array(
        'classes' => '',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'none',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_content_right|field_collection_item|field_services|default'] = $field_group;

  return $export;
}
