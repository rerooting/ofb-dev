<?php
/**
 * @file
 * ofb_base.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function ofb_base_user_default_roles() {
  $roles = array();

  // Exported role: Content Manager.
  $roles['Content Manager'] = array(
    'name' => 'Content Manager',
    'weight' => '2',
  );

  // Exported role: Site Manager.
  $roles['Site Manager'] = array(
    'name' => 'Site Manager',
    'weight' => '3',
  );

  return $roles;
}
