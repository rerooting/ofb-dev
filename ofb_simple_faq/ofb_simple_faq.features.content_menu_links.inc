<?php
/**
 * @file
 * ofb_simple_faq.features.content_menu_links.inc
 */

/**
 * Implements hook_content_menu_links_defaults().
 */
function ofb_simple_faq_content_menu_links_defaults() {
  $menu_links = array();

  // Exported menu link: main-menu:faq
  $menu_links['main-menu:faq'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'faq',
    'router_path' => 'faq',
    'link_title' => 'FAQ',
    'options' => array(
      'attributes' => array(
        'title' => 'Frequently Asked Questions',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('FAQ');


  return $menu_links;
}
