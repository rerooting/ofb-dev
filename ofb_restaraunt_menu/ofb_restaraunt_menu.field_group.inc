<?php
/**
 * @file
 * ofb_restaraunt_menu.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function ofb_restaraunt_menu_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_accordion|field_collection_item|field_menu_section|form';
  $field_group->group_name = 'group_accordion';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_menu_section';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Accordion',
    'weight' => '2',
    'children' => array(
      0 => 'group_normal',
      1 => 'group_catering',
      2 => 'group_drinks',
      3 => 'group_sub_section',
    ),
    'format_type' => 'accordion',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'effect' => 'none',
        'classes' => '',
      ),
    ),
  );
  $export['group_accordion|field_collection_item|field_menu_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_catering|field_collection_item|field_menu_section|form';
  $field_group->group_name = 'group_catering';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_menu_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_accordion';
  $field_group->data = array(
    'label' => 'Style: Catering',
    'weight' => '10',
    'children' => array(
      0 => 'field_catering_items',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_catering|field_collection_item|field_menu_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_drinks|field_collection_item|field_menu_section|form';
  $field_group->group_name = 'group_drinks';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_menu_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_accordion';
  $field_group->data = array(
    'label' => 'Style: Drinks',
    'weight' => '11',
    'children' => array(
      0 => 'field_drink_items',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_drinks|field_collection_item|field_menu_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_normal|field_collection_item|field_menu_section|form';
  $field_group->group_name = 'group_normal';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_menu_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_accordion';
  $field_group->data = array(
    'label' => 'Style: Normal',
    'weight' => '9',
    'children' => array(
      0 => 'field_normal_items',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_normal|field_collection_item|field_menu_section|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sub_section|field_collection_item|field_menu_section|form';
  $field_group->group_name = 'group_sub_section';
  $field_group->entity_type = 'field_collection_item';
  $field_group->bundle = 'field_menu_section';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_accordion';
  $field_group->data = array(
    'label' => 'Style: Sub Section',
    'weight' => '12',
    'children' => array(
      0 => 'field_item_type',
    ),
    'format_type' => 'accordion-item',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_sub_section|field_collection_item|field_menu_section|form'] = $field_group;

  return $export;
}
