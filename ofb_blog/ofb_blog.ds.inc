<?php
/**
 * @file
 * ofb_blog.ds.inc
 */

/**
 * Implements hook_ds_custom_fields_info().
 */
function ofb_blog_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'full_name_with_link';
  $ds_field->label = 'Full Name - With Link';
  $ds_field->field_type = 5;
  $ds_field->entities = array(
    'bean' => 'bean',
    'node' => 'node',
  );
  $ds_field->properties = array(
    'code' => array(
      'value' => 'By: <a href="[node:author:url]" title="View [node:author:field_first_name] [node:author:field_last_name]\'s Profile">[node:author:field_first_name] [node:author:field_last_name]</a>',
      'format' => 'ds_code',
    ),
    'use_token' => 1,
  );
  $export['full_name_with_link'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function ofb_blog_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|blog_article|teaser';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'blog_article';
  $ds_layout->view_mode = 'teaser';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'header' => array(
        0 => 'title',
      ),
      'left' => array(
        0 => 'field_primary_image',
        1 => 'full_name_with_link',
        2 => 'post_date',
        3 => 'links',
      ),
      'right' => array(
        0 => 'body',
      ),
    ),
    'fields' => array(
      'title' => 'header',
      'field_primary_image' => 'left',
      'full_name_with_link' => 'left',
      'post_date' => 'left',
      'links' => 'left',
      'body' => 'right',
    ),
    'classes' => array(),
  );
  $export['node|blog_article|teaser'] = $ds_layout;

  return $export;
}
