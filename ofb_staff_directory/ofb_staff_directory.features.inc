<?php
/**
 * @file
 * ofb_staff_directory.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_staff_directory_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ofb_staff_directory_node_info() {
  $items = array(
    'staff_directory' => array(
      'name' => t('Staff Directory'),
      'base' => 'node_content',
      'description' => t('List your team members here.'),
      'has_title' => '1',
      'title_label' => t('Staff Directory Title'),
      'help' => '',
    ),
  );
  return $items;
}
