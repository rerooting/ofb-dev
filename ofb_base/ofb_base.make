api = 2
core = 7.x





projects[less][type] = module
projects[less][subdir] = contrib




; Libraries ==================================================================


libraries[ckeditor][download][type]= "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.3/ckeditor_3.6.3.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

libraries[lessphp][download][type]= "get"
libraries[lessphp][download][url] = "http://leafo.net/lessphp/src/lessphp-0.3.4-2.tar.gz"
libraries[lessphp][directory_name] = "lessphp"
libraries[lessphp][destination] = "libraries"
