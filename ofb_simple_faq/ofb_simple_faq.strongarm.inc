<?php
/**
 * @file
 * ofb_simple_faq.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function ofb_simple_faq_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_frequently_asked_question';
  $strongarm->value = 0;
  $export['comment_anonymous_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_frequently_asked_question';
  $strongarm->value = 1;
  $export['comment_default_mode_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_frequently_asked_question';
  $strongarm->value = '50';
  $export['comment_default_per_page_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_frequently_asked_question';
  $strongarm->value = 1;
  $export['comment_form_location_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_frequently_asked_question';
  $strongarm->value = '1';
  $export['comment_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_frequently_asked_question';
  $strongarm->value = '1';
  $export['comment_preview_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_frequently_asked_question';
  $strongarm->value = 1;
  $export['comment_subject_field_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_frequently_asked_question';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_frequently_asked_question';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_frequently_asked_question';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_frequently_asked_question';
  $strongarm->value = '1';
  $export['node_preview_frequently_asked_question'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_frequently_asked_question';
  $strongarm->value = 0;
  $export['node_submitted_frequently_asked_question'] = $strongarm;

  return $export;
}
