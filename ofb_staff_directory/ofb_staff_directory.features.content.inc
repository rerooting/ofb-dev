<?php
/**
 * @file
 * ofb_staff_directory.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function ofb_staff_directory_content_defaults() {
  $content = array();

  $content['sample-staff-directory'] = (object) array(
    'exported_path' => 'content/staff',
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/sample-staff-directory',
      'router_path' => 'node/%',
      'link_title' => 'Staff',
      'options' => array(),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-48',
      'identifier' => 'main-menu:node-name/sample-staff-directory',
    ),
    'title' => 'Staff',
    'status' => '1',
    'promote' => '1',
    'sticky' => '0',
    'type' => 'staff_directory',
    'language' => 'und',
    'created' => '1338574314',
    'comment' => '0',
    'translate' => '0',
    'machine_name' => 'sample-staff-directory',
    'body' => array(),
    'field_staff_entry' => array(
      'und' => array(
        0 => array(
          'value' => '13',
        ),
        1 => array(
          'value' => '14',
        ),
        2 => array(
          'value' => '15',
        ),
        3 => array(
          'value' => '16',
        ),
        4 => array(
          'value' => '17',
        ),
        5 => array(
          'value' => '18',
        ),
      ),
    ),
  );

return $content;
}
