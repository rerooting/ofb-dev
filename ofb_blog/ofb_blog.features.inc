<?php
/**
 * @file
 * ofb_blog.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_blog_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ofb_blog_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ofb_blog_node_info() {
  $items = array(
    'blog_article' => array(
      'name' => t('Blog Article'),
      'base' => 'node_content',
      'description' => t('This is a blog article that will show up in your blog feed.'),
      'has_title' => '1',
      'title_label' => t('Post Title'),
      'help' => '',
    ),
  );
  return $items;
}
