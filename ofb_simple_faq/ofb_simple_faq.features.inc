<?php
/**
 * @file
 * ofb_simple_faq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_simple_faq_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function ofb_simple_faq_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function ofb_simple_faq_node_info() {
  $items = array(
    'frequently_asked_question' => array(
      'name' => t('Frequently Asked Question'),
      'base' => 'node_content',
      'description' => t('A frequently asked question to go into the FAQ section'),
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
  );
  return $items;
}
