<?php
/**
 * @file
 * ofb_base.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function ofb_base_content_defaults() {
  $content = array();

  $content['about'] = (object) array(
    'exported_path' => 'content/about',
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/about',
      'router_path' => 'node/%',
      'link_title' => 'About',
      'options' => array(
        'attributes' => array(
          'title' => 'About this wonderful OFB using biznass!',
        ),
      ),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '-49',
      'identifier' => 'main-menu:node-name/about',
    ),
    'title' => 'About',
    'status' => '1',
    'promote' => '0',
    'sticky' => '0',
    'type' => 'normal_page',
    'language' => 'und',
    'created' => '1338448437',
    'comment' => '0',
    'translate' => '0',
    'machine_name' => 'about',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => '<p>Lorem ipsum risus egestas amet elementum commodo donec justo, posuere ut felis pulvinar nibh scelerisque ullamcorper est, neque viverra fames donec aliquam suspendisse molestie nisi metus bibendum tristique curabitur potenti.</p><p>Quisque morbi integer quam himenaeos nulla vitae, sem tempor quis class volutpat quisque, pretium urna erat interdum eleifend curae inceptos lacinia ultricies commodo aliquam nisl a ultricies, purus ut vel nulla nostra pellentesque.</p><p>Et nam porttitor consequat urna ac viverra sagittis pharetra, integer sed taciti porta scelerisque nec himenaeos ut ac, torquent nullam risus et quisque gravida luctus.</p><p>Fringilla blandit tempus posuere condimentum semper class mollis, dui lacus elit quisque dapibus nunc ornare, iaculis lorem fermentum laoreet vehicula cubilia, in massa mattis netus at leo potenti phasellus nibh pellentesque pharetra senectus, arcu libero proin nisl lacinia massa, semper congue leo platea.</p><p>Vestibulum ante commodo metus viverra habitant aliquam leo hac aliquet diam conubia condimentum curae, pharetra semper aliquet ut nunc conubia placerat feugiat cubilia dictumst eleifend.</p><p>Id diam quam consectetur suscipit magna sit lobortis sollicitudin rhoncus, aenean cursus metus quisque scelerisque velit ultrices dolor arcu facilisis ullamcorper sagittis dictumst.</p><p>Quisque nullam curabitur torquent bibendum turpis imperdiet lacinia suspendisse, senectus torquent pretium quam malesuada adipiscing euismod lacus scelerisque, pretium dolor in faucibus taciti orci lacinia nam litora vel faucibus curabitur consectetur varius adipiscing dapibus pellentesque, fusce etiam consectetur conubia nisi himenaeos nullam.</p>',
          'summary' => '',
          'format' => 'content_manager',
          'safe_value' => ' <p>Lorem ipsum risus egestas amet elementum commodo donec justo, posuere ut felis pulvinar nibh scelerisque ullamcorper est, neque viverra fames donec aliquam suspendisse molestie nisi metus bibendum tristique curabitur potenti.</p>
<p>Quisque morbi integer quam himenaeos nulla vitae, sem tempor quis class volutpat quisque, pretium urna erat interdum eleifend curae inceptos lacinia ultricies commodo aliquam nisl a ultricies, purus ut vel nulla nostra pellentesque.</p>
<p>Et nam porttitor consequat urna ac viverra sagittis pharetra, integer sed taciti porta scelerisque nec himenaeos ut ac, torquent nullam risus et quisque gravida luctus.</p>
<p>Fringilla blandit tempus posuere condimentum semper class mollis, dui lacus elit quisque dapibus nunc ornare, iaculis lorem fermentum laoreet vehicula cubilia, in massa mattis netus at leo potenti phasellus nibh pellentesque pharetra senectus, arcu libero proin nisl lacinia massa, semper congue leo platea.</p>
<p>Vestibulum ante commodo metus viverra habitant aliquam leo hac aliquet diam conubia condimentum curae, pharetra semper aliquet ut nunc conubia placerat feugiat cubilia dictumst eleifend.</p>
<p>Id diam quam consectetur suscipit magna sit lobortis sollicitudin rhoncus, aenean cursus metus quisque scelerisque velit ultrices dolor arcu facilisis ullamcorper sagittis dictumst.</p>
<p>Quisque nullam curabitur torquent bibendum turpis imperdiet lacinia suspendisse, senectus torquent pretium quam malesuada adipiscing euismod lacus scelerisque, pretium dolor in faucibus taciti orci lacinia nam litora vel faucibus curabitur consectetur varius adipiscing dapibus pellentesque, fusce etiam consectetur conubia nisi himenaeos nullam.</p>
 ',
          'safe_summary' => '  ',
        ),
      ),
    ),
  );

return $content;
}
