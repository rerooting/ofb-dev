<?php
/**
 * @file
 * ofb_base.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ofb_base_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function ofb_base_node_info() {
  $items = array(
    'normal_page' => array(
      'name' => t('Normal Page'),
      'base' => 'node_content',
      'description' => t('This is a normal page that is treated as an independent piece of content.  Try page_plus for more enhancements for the normal page!'),
      'has_title' => '1',
      'title_label' => t('Page Title'),
      'help' => t('The title of your page will appear at the top of the page, in the title on your browser and, by default, the URL.'),
    ),
  );
  return $items;
}
