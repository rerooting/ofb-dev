<?php
/**
 * @file
 * ofb_restaraunt_menu.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function ofb_restaraunt_menu_content_defaults() {
  $content = array();

  $content['cafe-menu'] = (object) array(
    'exported_path' => 'heartland-point/cafe/take-out-menu',
    'link' => array(
      'menu_name' => 'main-menu',
      'link_path' => 'node-name/cafe-menu',
      'router_path' => 'node/%',
      'link_title' => 'Take-Out Menu',
      'options' => array(),
      'module' => 'menu',
      'hidden' => '0',
      'external' => '0',
      'has_children' => '0',
      'expanded' => '0',
      'weight' => '0',
      'parent_path' => 'node/12',
      'identifier' => 'main-menu:node-name/cafe-menu',
    ),
    'title' => 'Take-Out Menu',
    'status' => '1',
    'promote' => '1',
    'sticky' => '0',
    'type' => 'cafe_menu',
    'language' => 'und',
    'created' => '1337484356',
    'comment' => '1',
    'translate' => '0',
    'machine_name' => 'cafe-menu',
    'field_menu_section' => array(
      'und' => array(
        0 => array(
          'value' => '8',
        ),
        1 => array(
          'value' => '15',
        ),
        2 => array(
          'value' => '16',
        ),
      ),
    ),
  );

return $content;
}
