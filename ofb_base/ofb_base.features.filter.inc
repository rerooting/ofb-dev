<?php
/**
 * @file
 * ofb_base.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function ofb_base_filter_default_formats() {
  $formats = array();

  // Exported format: Content Manager
  $formats['content_manager'] = array(
    'format' => 'content_manager',
    'name' => 'Content Manager',
    'cache' => '1',
    'status' => '1',
    'weight' => '0',
    'filters' => array(
      'filter_html' => array(
        'weight' => '-10',
        'status' => '1',
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <pre> <code> <p> <ul> <ol> <li> <dl> <dt> <dd> <img> <iframe> <table> <td> <th> <tr> <tbody> <tfoot> <audio> <video> <h1> <h2> <h3> <h4> <h5> <h6> <h7> <sub> <sup> <embed> <header> ',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 1,
        ),
      ),
      'filter_autop' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => '0',
        'status' => '1',
        'settings' => array(
          'filter_url_length' => '72',
        ),
      ),
      'media_filter' => array(
        'weight' => '2',
        'status' => '1',
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => '10',
        'status' => '1',
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
