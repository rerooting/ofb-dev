<?php
/**
 * @file
 * ofb_contact.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function ofb_contact_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'contact_infobean';
  $bean_type->label = 'Contact InfoBean';
  $bean_type->options = '';
  $bean_type->description = 'A structured contact info bean';
  $export['contact_infobean'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'map_bean';
  $bean_type->label = 'Map Bean';
  $bean_type->options = '';
  $bean_type->description = 'A bean with a map inside!';
  $export['map_bean'] = $bean_type;

  return $export;
}
